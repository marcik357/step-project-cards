import { cards } from "./vars";

// These two variables will store the card that is being dragged and the card it is being dropped on
let draggedCard = null;
let droppedCard = null;

// This function is responsible for enabling drag and drop functionality
export function dragDrop() {
  const draggableCards = document.querySelectorAll('.card');

  // Loop through each card and add event listeners
  draggableCards.forEach(card => {
    card.addEventListener('dragstart', handlerDragStart);
    card.addEventListener('dragend', handlerDragEnd);
    card.addEventListener('dragenter', handlerDragEnter);
    card.addEventListener('dragleave', (e) => e.target === droppedCard ? droppedCard = null : null);
  });
  
  cards.addEventListener('dragover',(e) =>  e.preventDefault() );
  cards.addEventListener('drop', handlerDrop);
}

// This function is called when a card starts being dragged
function handlerDragStart(e) {
  // Set the 'draggedCard' variable to the current card
  draggedCard = this;

  // Add the 'active' class to the current card
  this.classList.add('active');
}

// This function is called when a card finishes being dragged
function handlerDragEnd(e) {
  // Reset the 'draggedCard' variable to null
  draggedCard = null;

  // Remove the 'active' class from the current card
  this.classList.remove('active');
}

// This function is called when a card is dropped on another card
function handlerDrop(e) {
  // If the 'droppedCard' variable is null, set it to the last child of the current card
  if (droppedCard === null) {
    const children = Array.from(this.children);
    droppedCard = children[children.length - 1];
  }

  // If the 'droppedCard' variable is not null, get the index of the dragged card and dropped card
  if (droppedCard !== null) {
    const children = Array.from(droppedCard.parentElement.children);
    const draggedIndex = children.indexOf(draggedCard);
    const droppedIndex = children.indexOf(droppedCard);

    // If the dragged card is being dropped before the dropped card, insert it before the dropped card
    if (draggedIndex > droppedIndex) {
      draggedCard.parentElement.insertBefore(draggedCard, droppedCard);
    } else {
      // Otherwise, insert it after the dropped card
      draggedCard.parentElement.insertBefore(draggedCard, droppedCard.nextElementSibling);
    }
  } else {
    // If the 'droppedCard' variable is null, append the dragged card to the current card
    this.append(draggedCard);
  }

  // Remove the 'active' class from the 'cards' variable
  cards.classList.remove('active');
}

// This function is called when a card enters another card during a drag operation
function handlerDragEnter(e) {
  // If the dragged card is the same as the dropped card, set the 'droppedCard' variable to null
  draggedCard === droppedCard ? null : droppedCard = this;
}

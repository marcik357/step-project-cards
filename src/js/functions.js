import { Component } from "./Component.js";
import { app, cards, visitType, formType, pageElements } from "./vars.js";
import { Modal } from "./Modal.js";
import { VisitForm } from "./Form.js";
import { DoctorAPIService } from "./DoctorAPIService.js";

export function toggleNoCardsMessage() {
    if (cards.querySelector(getClassName(pageElements.card.classList))) {
        // Delete no cards message
        Component.deleteElement('.no-cards')
    } else if (!cards.querySelector('.no-cards')) {
        // Add no cards message
        cards.append(new Component(pageElements.noCardsMessage).createHTMLElement())
    }
}

export async function showCards() {
    try {
        cards.innerHTML = ''
        // Show loader
        cards.prepend(showLoader())
        // Get all cards
        const data = await new DoctorAPIService().getAllCards();
        // Insert cards on page
        data.forEach(obj => {
            let VisitClass = visitType[obj.doctor];
            const newCard = new VisitClass(obj).addCard()
            cards.append(newCard)
        })
        // Start message
        toggleNoCardsMessage()
        // }
    } catch (error) {
        console.error(error)
    }
}

export const showLoader = () => new Component(pageElements.loader).createHTMLElement()

export const deleteLoader = () => Component.deleteElement(getClassName(pageElements.loader.classList))

export function getClassName(classList) {
    if (typeof classList === 'string') {
        return '.' + classList
    } else if (classList instanceof Array) {
        return '.' + classList.join('.')
    }
}
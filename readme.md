# [**step-project-cards**](https://marcik357.gitlab.io/step-project-cards/) - Step project after module Advanced JavaScript.
The task was to implement a web-page where the person could create cards describing planned visits to doctors.
We had to apply ES6 classes, fetch and ES module structure.
Before log in you have to register here https://ajax.test-danit.com/front-pages/cards-register.html



## The following technologies were used when working on the project:

* JavaScript: classes, modules, functions, object destructuring;
* LocalStorage: Client-side data storage;
* DOM: Creating HTML elements via JavaScript;
* Asynchrony: Use async/await to manage asynchronous requests;
* Modular structure: organization of code through modules;
* BEM class naming methodology;
* HTML using semantic tags;
* CSS preprocessor SASS SCSS:
    * _Variables_
    * _Mixins_
    * _Functions_
    * _File separation and import_
* Adaptive and responsive design (for desktop and tablets);
* The project was compiled based on Vite;
---

## Project developers:
* ___Mykhailichenko Anton___
* ___Chekh Liliia___

---

## List of completed works:
* ___Mykhailichenko Anton___:
    * Creating a vite assembly
    * HTML & CSS design
    * Class Component
    * Class DoctorAPIService (50/50)
    * Class Form and child classes
    * Class Modal
    * Authorization and validation
    * Visit filtering logic
   

* ___Chekh Liliia___: 
    * Class Visit and child classes
    * Class DoctorAPIService (50/50)
    * Drag&Drop
    * Loader
    * Readme
   

---   

## Basic commands:
    1. npm i - project initialization
    2. npm run build - make a production-ready build 
    3. npm run dev - launch in development mode


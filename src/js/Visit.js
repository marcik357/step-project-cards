import { Component } from "./Component.js";
import { toggleNoCardsMessage, deleteLoader } from "./functions.js";
import { DoctorAPIService } from "./DoctorAPIService.js";
import { app, newKeys, createCardRow, formType, keysForValue, pageElements } from "./vars.js"
import { Modal } from "./Modal.js";
import { EditVisitForm } from "./Form.js"

export class Visit {
  constructor({ id, fullName, doctor, goal, description, urgency, status = 'open' }) {
    this.id = id;
    this.fullName = fullName;
    this.doctor = doctor;
    this.goal = goal;
    this.description = description;
    this.urgency = urgency;
    this.status = status
  }
  // Create a new card element
  addCard() {
    // Create new card
    const card = new Component({ ...pageElements.card, attr: [{ 'data-id': this.id }, { 'data-status': this.status }, { 'draggable': 'true' }] }).createHTMLElement();
    // insert content in card
    card.insertAdjacentHTML('afterbegin', `
      <div class="card__content">
        ${this.createCardContent()}
      </div>
    `);
    // Insert delete btn
    const delBtn = new Component({ ...pageElements.cardDelBtn, listener: this.deleteCard.bind(card) }).createHTMLElement();
    card.append(delBtn);
    // Insert show more btn
    const showMoreBtn = new Component({ ...pageElements.cardShowMoreBtn, listener: this.showMore.bind(card) }).createHTMLElement()
    card.append(showMoreBtn)
    // Insert edit btn
    const editBtn = new Component({ ...pageElements.cardEditBtn, listener: this.editCard.bind(this) }).createHTMLElement()
    card.append(editBtn)
    // Isert card
    return card;
  }

  // Toggle the display of additional content in the card
  showMore() {
    this.classList.toggle('open');
  }

  // Delete card
  deleteCard() {
    if (new DoctorAPIService().deleteCard(this.dataset.id)) {
      this.remove();
      toggleNoCardsMessage()
    }
  }

  // Edit card
  editCard() {
    app.prepend(new Modal(formType.editing).showModal(new EditVisitForm({ ...formType.editing, card: this, ...pageElements.baseForm }).showForm()))
  }

  // Create content for card
  createCardContent() {
    let fieldsHtml = '';
    // Create card content
    Object.entries(this).forEach(([key, value]) => {
      if (newKeys[key]) {
        const val = keysForValue[value] ? keysForValue[value] : value;
        fieldsHtml += createCardRow(newKeys[key], val)
      }
    });
    // Insert card content
    return fieldsHtml;
  };
}

export class VisitDentist extends Visit {
  constructor({ id, fullName, doctor, goal, description, urgency, lastVisit, status }) {
    super({ id, fullName, doctor, goal, description, urgency, status });

    this.lastVisit = lastVisit;
  }
}

export class VisitCardiologist extends Visit {
  constructor({ id, fullName, doctor, goal, description, urgency, pressure, bodyIndex, cardioDiseases, age, status }) {
    super({ id, fullName, doctor, goal, description, urgency, status });

    this.pressure = pressure;
    this.bodyIndex = bodyIndex;
    this.cardioDiseases = cardioDiseases;
    this.age = age;
  }
}

export class VisitTherapist extends Visit {
  constructor({ id, fullName, doctor, goal, description, urgency, age, status }) {
    super({ id, fullName, doctor, goal, description, urgency, status });

    this.age = age;
  }
}
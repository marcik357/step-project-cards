import { Component } from "./Component.js";
import { app, cards, formType, pageElements } from "./vars.js";
import { showCards, getClassName, deleteLoader } from "./functions.js";
import { Modal } from "./Modal.js";
import { LoginForm, VisitForm, FilterVisitForm } from "./Form.js";
import { dragDrop } from "./Drag_drop.js";

export async function checkLogin() {
    if (localStorage.getItem('token')) {
        // Replace elements
        const logInElem = document.querySelector(`#${pageElements.logInBtn.attr.find(obj => obj.hasOwnProperty('id')).id}`);
        logInElem.after(new Component({ ...pageElements.logOutBtn, listener: logOut }).createHTMLElement())
        logInElem.after(new Component({ ...pageElements.createVisitBtn, listener: createVisit }).createHTMLElement())
        logInElem.remove()
        app.querySelector('.filter-section__container').prepend(new FilterVisitForm(pageElements.filterForm).showForm())
        // Show cards
        await showCards()
        // Remove loader
        deleteLoader()
        // Init Drag-n-Drop
        dragDrop()
    } else {
        // Set listener on log in btn
        document.querySelector(`#${pageElements.logInBtn.attr.find(obj => obj.hasOwnProperty('id')).id}`).addEventListener('click', logIn)
    }
}

function logIn() {
    // Show log in form
    app.prepend(new Modal(formType.login).showModal(new LoginForm({ ...formType.login, ...pageElements.baseForm }).showForm()))
}

function logOut(e) {
    // Clear page and token
    localStorage.removeItem('token')
    Component.deleteElement(getClassName(pageElements.filterForm.classList))
    cards.innerHTML = pageElements.noUser
    // Replace log in btn by log out
    e.target.replaceWith(new Component({ ...pageElements.logInBtn, listener: logIn }).createHTMLElement())
    document.querySelector(`#${pageElements.createVisitBtn.attr.find(obj => obj.hasOwnProperty('id')).id}`).remove()
}

function createVisit() {
    // Insert creat visit form
    app.prepend(new Modal(formType.visit).showModal(new VisitForm({ ...formType.visit, ...pageElements.baseForm }).showForm()))
}
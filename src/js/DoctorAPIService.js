import { urlAPI } from './vars.js'

export class DoctorAPIService {
    constructor() {}

    async logIn(reqBody) {
        try {
            // Send login request
            const response = await fetch(urlAPI + 'login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(reqBody)
            })
            // In bad case throw error
            if (!response.ok) {
                throw new Error('Incorrect username or password');
            }
            // Write toket in local storage
            const token = await response.text()
            localStorage.setItem('token', token)
        } catch (error) {
            throw new Error(error.message)
        }
    }

    async createCard(reqBody) {
        try {
            // Send create card request
            const response = await fetch(urlAPI, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify(reqBody)
            })
            // In bad case throw error
            if (!response.ok) {
                throw new Error('Incorrect username or password')
            }
            // Return server answer
            const data = await response.json()
            return data
        } catch (error) {
            console.error(error.message)
        }
    }

    async editCard(cardId, reqBody) {
        try {
            // Send editing card request
            const response = await fetch(urlAPI + cardId, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify(reqBody)
            })
            // In bad case throw error
            if (!response.ok) {
                throw new Error('Incorrect card ID');
            }
            // Return server answer
            const data = response.json()
            return data
        } catch (error) {
            console.error(error.message)
        }
    }

    async getAllCards() {
        try {
            // Send request to get all cards
            const response = await fetch("https://ajax.test-danit.com/api/v2/cards/", {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            // In bad case throw error
            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
            // Return server answer
            const data = await response.json()
            return data
        } catch (error) {
            console.error(error.message)
        }
    }

    async deleteCard(cardId) {
        try {
            // Send delete request
            const response = await fetch(`${urlAPI}${cardId}`, {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            // In bad case throw error
            if (!response.ok) {
                throw new Error('Incorrect card ID');
            }
            // Return server answer
            return true
        } catch (error) {
            console.error(error.message)
        }
    }
}

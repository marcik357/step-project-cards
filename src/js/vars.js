import { VisitDentist, VisitCardiologist, VisitTherapist } from "./Visit.js"

export const app = document.querySelector('#app')
export const cards = document.querySelector('.cards')
export const urlAPI = "https://ajax.test-danit.com/api/v2/cards/"

export const createCardRow = (name, value) => `<p class="card__text">${name}: <span class="card__desc">${value}</span></p>`

const visit = {
    Cardiologist: {
        pressureLabel: {
            tagName: 'label',
            classList: 'form__label',
            value: 'Звичайний тиск:',
            attr: [{ 'for': 'pressure' }],
            listener: '',
            event: ''
        },
        pressure: {
            tagName: 'input',
            classList: 'form__input',
            value: '',
            attr: [{ 'id': 'pressure' }, { 'name': 'pressure' }, { 'type': 'text' }, { 'required': '' }, { 'placeholder': 'у форматі 000/000' }],
            listener: (e) => { if (isNaN(e.key) && e.key !== '/') e.preventDefault() },
            event: 'keypress'
        },
        bodyIndexLabel: {
            tagName: 'label',
            classList: 'form__label',
            value: 'Індекс маси тіла:',
            attr: [{ 'for': 'bodyIndex' }],
            listener: '',
            event: ''
        },
        bodyIndex: {
            tagName: 'input',
            classList: 'form__input',
            value: '',
            attr: [{ 'id': 'bodyIndex' }, { 'name': 'bodyIndex' }, { 'type': 'number' }, { 'step': '0.01' }, { 'required': '' }, { 'placeholder': 'наприклад 18.5 або 30' }],
            listener: '',
            event: ''
        },
        cardioDiseasesLabel: {
            tagName: 'label',
            classList: 'form__label',
            value: 'Перенесені захворювання серцево-судинної системи:',
            attr: [{ 'for': 'cardioDiseases' }],
            listener: '',
            event: ''
        },
        cardioDiseases: {
            tagName: 'input',
            classList: 'form__input',
            value: '',
            attr: [{ 'id': 'cardioDiseases' }, { 'name': 'cardioDiseases' }, { 'type': 'text' }, { 'required': '' }, { 'placeholder': 'Перерахуйте, або "немає"' }],
            listener: '',
            event: ''
        },
        ageLabel: {
            tagName: 'label',
            classList: 'form__label',
            value: 'Вік пацієнта:',
            attr: [{ 'for': 'age' }],
            listener: '',
            event: ''
        },
        age: {
            tagName: 'input',
            classList: 'form__input',
            value: '',
            attr: [{ 'id': 'age' }, { 'name': 'age' }, { 'type': 'number' }, { 'required': '' }, { 'placeholder': '00' }],
            listener: '',
            event: ''
        },
        btn: {
            tagName: 'button',
            classList: ['form__btn', 'btn-main'],
            value: 'Зберегти',
            attr: [{ 'id': 'create' }, { 'type': 'submit' }],
            listener: '',
            event: ''
        }
    },
    //====================================================================================================
    Dentist: {
        lastVisitLabel: {
            tagName: 'label',
            classList: 'form__label',
            value: 'Дата останнього відвідування:',
            attr: [{ 'for': 'lastVisit' }],
            listener: '',
            event: ''
        },
        lastVisit: {
            tagName: 'input',
            classList: 'form__input',
            value: '',
            attr: [{ 'id': 'lastVisit' }, { 'name': 'lastVisit' }, { 'type': 'date' }, { 'required': '' }],
            listener: '',
            event: ''
        },
        btn: {
            tagName: 'button',
            classList: ['form__btn', 'btn-main'],
            value: 'Зберегти',
            attr: [{ 'id': 'create' }, { 'type': 'submit' }],
            listener: '',
            event: ''
        }
    },
    //====================================================================================================
    Therapist: {
        ageLabel: {
            tagName: 'label',
            classList: 'form__label',
            value: 'Вік пацієнта:',
            attr: [{ 'for': 'age' }],
            listener: '',
            event: ''
        },
        age: {
            tagName: 'input',
            classList: 'form__input',
            value: '',
            attr: [{ 'id': 'age' }, { 'name': 'age' }, { 'type': 'number' }, { 'required': '' }, { 'placeholder': '00' }],
            listener: '',
            event: ''
        },
        btn: {
            tagName: 'button',
            classList: ['form__btn', 'btn-main'],
            value: 'Зберегти',
            attr: [{ 'id': 'create' }, { 'type': 'submit' }],
            listener: '',
            event: ''
        }
    },
}

const login = {
    emailLabel: {
        tagName: 'label',
        classList: 'form__label',
        value: 'Введіть ваш email:',
        attr: [{ 'for': 'email' }],
        listener: '',
        event: ''
    },
    email: {
        tagName: 'input',
        classList: 'form__input',
        value: '',
        attr: [{ 'id': 'email' }, { 'name': 'email' }, { 'type': 'text' }, { 'required': '' }, { 'placeholder': 'example@mail.com' }],
        listener: '',
        event: ''
    },
    passwordLabel: {
        tagName: 'label',
        classList: 'form__label',
        value: 'Введіть ваш пароль:',
        attr: [{ 'for': 'password' }],
        listener: '',
        event: ''
    },
    password: {
        tagName: 'input',
        classList: 'form__input',
        value: '',
        attr: [{ 'id': 'password' }, { 'name': 'password' }, { 'type': 'password' }, { 'required': '' }, { 'placeholder': '******' }],
        listener: '',
        event: ''
    },
    btn: {
        tagName: 'button',
        classList: ['form__btn', 'btn-main'],
        value: 'Увійти',
        attr: [{ 'id': 'login' }, { 'type': 'submit' }],
        listener: '',
        event: ''
    }
}

const regularFields = {
    goalLabel: {
        tagName: 'label',
        classList: 'form__label',
        value: 'Мета візиту:',
        attr: [{ 'for': 'goal' }],
        listener: '',
        event: ''
    },
    goal: {
        tagName: 'input',
        classList: 'form__input',
        value: '',
        attr: [{ 'id': 'goal' }, { 'name': 'goal' }, { 'type': 'text' }, { 'required': '' }, { 'placeholder': 'Мета візиту' }],
        listener: '',
        event: ''
    },
    descriptionLabel: {
        tagName: 'label',
        classList: 'form__label',
        value: 'Короткий опис візиту:',
        attr: [{ 'for': 'description' }],
        listener: '',
        event: ''
    },
    description: {
        tagName: 'input',
        classList: 'form__input',
        value: '',
        attr: [{ 'id': 'description' }, { 'name': 'description' }, { 'type': 'text' }, { 'placeholder': 'Короткий опис візиту' }],
        listener: '',
        event: ''
    },
    urgency: {
        tagName: 'select',
        classList: 'form__select',
        value: `
                        <option disabled selected value="">Оберіть терміновість</option>
                        <option value="high">Невідкладна</option>
                        <option value="normal">Пріоритетна</option>
                        <option value="low">Звичайна</option>
                    `,
        attr: [{ 'id': 'urgency' }, { 'name': 'urgency' }, { 'required': '' }],
        listener: '',
        event: ''
    },
    fullNameLabel: {
        tagName: 'label',
        classList: 'form__label',
        value: 'ПІБ:',
        attr: [{ 'for': 'fullName' }],
        listener: '',
        event: ''
    },
    fullName: {
        tagName: 'input',
        classList: 'form__input',
        value: '',
        attr: [{ 'id': 'fullName' }, { 'name': 'fullName' }, { 'type': 'text' }, { 'required': '' }, { 'placeholder': 'Шевченко Тарас Григорович' }],
        listener: '',
        event: ''
    },
}

export const formType = {
    login: {
        content: login,
        title: 'Увійти в систему'
    },

    visit: {
        regularFields: regularFields,
        content: visit,
        title: 'Створити візит'
    },
    editing: {
        regularFields: {
            ...regularFields,
            statusBlock: {
                tagName: 'p',
                classList: 'form__label',
                value: 'Статус візиту:',
                attr: undefined,
                listener: '',
                event: ''
            },
            statusLabel: {
                tagName: 'label',
                classList: 'form__label',
                value: 'Ще не відбувся',
                attr: [{ 'for': 'open' }],
                listener: '',
                event: ''
            },
            status: {
                tagName: 'input',
                classList: 'form__radio',
                value: 'open',
                attr: [{ 'id': 'open' }, { 'name': 'status' }, { 'type': 'radio' }],
                listener: '',
                event: ''
            },
            statusLabel2: {
                tagName: 'label',
                classList: 'form__label',
                value: 'Завершений',
                attr: [{ 'for': 'done' }],
                listener: '',
                event: ''
            },
            status2: {
                tagName: 'input',
                classList: 'form__radio',
                value: 'done',
                attr: [{ 'id': 'done' }, { 'name': 'status' }, { 'type': 'radio' }],
                listener: '',
                event: ''
            }
        },
        content: visit,
        title: 'Редагувати візит'
    }
}

export const visitType = {
    Dentist: VisitDentist,
    Cardiologist: VisitCardiologist,
    Therapist: VisitTherapist,
};

export const newKeys = {
    fullName: 'П.І.Б.',
    doctor: 'Лікар',
    goal: 'Мета візиту',
    description: 'Короткий опис візиту',
    urgency: 'Терміновість',
    lastVisit: 'Дата останнього відвідування',
    pressure: 'Звичайний тиск',
    bodyIndex: 'Індекс маси тіла',
    cardioDiseases: 'Перенесені захворювання серцево-судинної системи',
    age: 'Вік'
}

export const regExp = {
    goal: /^[\p{L}\d]{3,}/u,
    fullName: /^([\p{L}\d]{2,}\s){1,}[\p{L}\d]{2,}$/u,
    pressure: /^(?:5[0-9]|6[0-9]|7[0-9]|8[0-9]|9[0-9]|1[0-9][0-9]|2[0-5][0-9])\/(?:5[0-9]|6[0-9]|7[0-9]|8[0-9]|9[0-9]|1[0-9][0-9]|2[0-5][0-9])$/,
    bodyIndex: /^(?:\d{2}|(\d{2}[.,]\d{1,2}))$/,
    cardioDiseases: /^[\p{L}\d, \-]{3,}$/u,
    age: /^(1\d{0,2}|2\d{0,1}|[1-9]\d?)$|^200$/,
    email: /^[\w-.]+@([\w-]+\.)+[\w-]{2,}$/,
}

export const keysForValue = {
    Dentist: 'Стоматолог',
    Cardiologist: 'Кардіолог',
    Therapist: 'Терапевт',
    high: 'Невідкладна',
    normal: 'Пріоритетна',
    low: 'Звичайна'
}

export const pageElements = {
    logInBtn: {
        tagName: 'button',
        classList: ['header__btn', 'btn-main'],
        value: 'Вхід',
        attr: [{ 'type': 'button' }, { 'id': 'logIn' }],
        listener: undefined,
        event: 'click'
    },
    logOutBtn: {
        tagName: 'button',
        classList: ['header__btn', 'btn-main'],
        value: 'Вийти',
        attr: [{ 'type': 'button' }, { 'id': 'logOutBtn' }],
        listener: undefined,
        event: 'click'
    },
    createVisitBtn: {
        tagName: 'button',
        classList: ['header__btn', 'btn-main'],
        value: 'Створити візит',
        attr: [{ 'type': 'button' }, { 'id': 'createVisitBtn' }],
        listener: undefined,
        event: 'click'
    },
    baseForm: {
        tagName: 'form',
        classList: ['modal__form', 'form'],
        value: '',
        attr: [],
        listener: '',
        event: 'submit'
    },
    visitFormSelect: {
        tagName: 'select',
        classList: 'form__select',
        value: `
                <option disabled selected value="">Оберіть лікаря</option>
                <option value="Cardiologist">Кардіолог</option>
                <option value="Dentist">Стоматолог</option>
                <option value="Therapist">Терапевт</option>
            `,
        attr: [{ 'id': 'doctor' }, { 'name': 'doctor' }],
        listener: undefined,
        event: 'change'
    },
    card: {
        tagName: 'div',
        classList: ['cards__item', 'card'],
        value: undefined,
        attr: undefined,
        listener: undefined,
        event: 'change'
    },
    cardDelBtn: {
        tagName: 'button',
        classList: 'card__del',
        value: undefined,
        attr: undefined,
        listener: undefined,
        event: 'click'
    },
    cardShowMoreBtn: {
        tagName: 'button',
        classList: ['card__show-more', 'btn-main'],
        value: 'Показати більше',
        attr: undefined,
        listener: undefined,
        event: 'click'
    },
    cardEditBtn: {
        tagName: 'button',
        classList: 'card__edit',
        value: '&#128393',
        attr: undefined,
        listener: undefined,
        event: 'click'
    },
    noCardsMessage: {
        tagName: 'p',
        classList: 'no-cards',
        value: 'No items have been added',
        attr: undefined,
        listener: undefined,
        event: undefined
    },
    noUser: `<p class="no-cards">Потрібно залогінитись</p>`,
    noMatches: 'Немає співпадінь',
    loader: {
        tagName: 'div',
        classList: 'loader',
        value: `<div class="pulse"></div>`,
        attr: undefined,
        listener: undefined,
        event: undefined
    },
    filterForm: {
            tagName: 'form',
            classList: ['filter-section__form', 'filter-form'],
            value: '',
            attr: [],
            listener: '',
            event: 'input',
            formContent: `
                <div class="filter-form__block filter-form__content">
                    <label class="filter-form__label form__content" for="content">Пошук за заголовком/вмістом візиту:</label>
                    <input class="filter-form__input" type="text" name="content" id="content">
                </div>
                <div class="filter-form__block filter-form__status">
                    <legend class="filter-form__legend">Статус:</legend>
                    <label class="filter-form__label" for="status-open">Open</label>
                    <input class="filter-form__radio" type="checkbox" name="status-open" value="open" id="status-open">
                    <label class="filter-form__label" for="status-done">Done</label>
                    <input class="filter-form__radio" type="checkbox" name="status-done" value="done" id="status-done">
                </div>
                <div class="filter-form__block filter-form__urgency">
                    <legend class="filter-form__legend">Термновість візиту:</legend>
                    <label class="filter-form__label" for="urgency-high">High</label>
                    <input class="filter-form__radio" type="checkbox" name="urgency-high" value="high" id="urgency-high">
                    <label class="filter-form__label" for="urgency-normal">Normal</label>
                    <input class="filter-form__radio" type="checkbox" name="urgency-normal" value="normal" id="urgency-normal">
                    <label class="filter-form__label" for="urgency-low">Low</label>
                    <input class="filter-form__radio" type="checkbox" name="urgency-low" value="low" id="urgency-low">
                </div>
                <button class="filter-form__reset btn-secondary" type="reset">Скасувати</button>
            `
    },
}
import { cards, visitType, regExp, pageElements } from "./vars.js";
import { Component } from "./Component.js";
import { checkLogin } from "./authorization.js";
import { toggleNoCardsMessage, showCards, showLoader, deleteLoader, getClassName  } from "./functions.js";
import { DoctorAPIService } from "./DoctorAPIService.js";
import { dragDrop } from "./Drag_drop.js";

class Form extends Component {
    constructor({ tagName = "form", classList, value, attr = [], listener, event = "submit" }) {
        super({ tagName, classList, value, attr, listener, event });
    }

    submit(e) {
        if (e) e.preventDefault()
        try {
            // Abort submit if invalide input
            if (this.querySelector('.error')) return undefined;
            // Show loader
            cards.prepend(showLoader())
            // Create obj to send
            const obj = {};
            [...this.children, ...this.querySelectorAll('input')].forEach(elem => {
                if (['SELECT', 'INPUT'].includes(elem.tagName)) {
                    if ((elem.type === 'radio' || elem.type === 'checkbox') && elem.checked) {
                        obj[elem.name] = elem.value
                    } else if (elem.type !== 'radio' && elem.type !== 'checkbox') {
                        obj[elem.name] = elem.value
                    }
                }
            })
            return obj
        } catch (error) {
            console.error(error);
        }
    }

    insertFields(propsObj, parent) {
        Object.values(propsObj).forEach(obj => parent.append(new Component(obj).createHTMLElement()))
    }

    selectCategory(select, e) {
        const category = e.target.value
        // Find form
        const form = this.findHTMLElement();
        // Clear form on change category
        [...form.children].forEach(elem => {
            if (elem.id !== select) elem.remove()
        })
        //Insert fields in form
        this.insertFields(this.regularContent, form)
        this.insertFields(this.content[category], form)
        this.setValidation(form, 'INPUT')
    }

    fillValues(template, data) {
        Object.entries(template).forEach(([key, value]) => {
            if (!value.value && data[key]) {
                value.value = data[key]
            }
        })
    }

    setSelected(form, selectId) {
        form.querySelectorAll(`#${selectId} option`).forEach(option => {
            if (option.value === this.card[`${selectId}`]) {
                option.setAttribute('selected', true)
            } else {
                option.removeAttribute('selected', true)
            }
        })
    }

    setValidation(form, fieldsType) {
        [...form.children].forEach(elem => {
            if (elem.tagName === fieldsType) {
                elem.addEventListener('change', () => {
                    if (regExp[elem.id] && !Component.validateData(regExp[elem.id], elem.value)) {
                        elem.classList.add('error')
                        elem.addEventListener('input', (e) => e.target.classList.remove('error'), { once: true })
                    }
                })
            }
        })
    }
}

export class LoginForm extends Form {
    constructor({ content, tagName, classList, value, attr, listener, event }) {
        super({ tagName, classList, value, attr, listener, event })
        this.content = content
    }

    showForm() {
        // Create form
        const form = this.createHTMLElement()
        form.addEventListener('submit', this.submit)
        //Insert fields in form
        this.insertFields(this.content, form)
        // Insert form
        return form
    }

    async submit(e) {
        e.preventDefault()
        try {
            // Create obj to send
            const obj = super.submit(e)
            if (!obj) return
            // Send request
            await new DoctorAPIService().logIn(obj)
            // Close modal
            this.closest('.modal').remove()
            // Replace login btn, show cards, etc.
            checkLogin()
        } catch (error) {
            // Remove loader
            deleteLoader()
            alert(error);
        }
    }
}

export class VisitForm extends Form {
    constructor({ regularFields, content, tagName, classList, value, attr, listener, event }) {
        super({ tagName, classList, value, attr, listener, event })
        this.regularContent = regularFields
        this.content = content
    }

    showForm() {
        // Create form
        const form = this.createHTMLElement()
        form.addEventListener(this.event, this.submit)
        // Insert category select in form
        const select = new Component({ ...pageElements.visitFormSelect, listener: this.selectCategory.bind(this, 'doctor') }).createHTMLElement()
        form.append(select)
        // Insert form
        return form
    }

    async submit(e) {
        try {
            // Create obj to send
            const obj = super.submit(e)
            // Abort if no data to send
            if (!obj) return
            // Close modal
            this.closest('.modal').remove()
            // Send request
            const data = await new DoctorAPIService().createCard({ ...obj, status: 'open' })
            // Create new card
            const Visit = await visitType[data.doctor]
            if (!Visit) {
                throw new Error('Unknown doctor type')
            }
            const newCard = new Visit(data).addCard()
            // Insert new card
            cards.append(newCard)
            // // Delete no cards message          
            toggleNoCardsMessage()
            // Clear filter form
            document.querySelector(getClassName(pageElements.filterForm.classList)).reset()
            // Delete loader
            deleteLoader()
        } catch (error) {
            console.error(error)
        }
    }
}

export class EditVisitForm extends Form {
    constructor({ regularFields, content, card, tagName, classList, value, attr, listener, event }) {
        super({ tagName, classList, value, attr, listener, event })
        this.regularContent = JSON.parse(JSON.stringify(regularFields))
        this.content = JSON.parse(JSON.stringify(content))
        this.card = card
    }

    selectCategory(select, e) {
        super.selectCategory(select, e)
        const form = this.findHTMLElement()
        this.setSelected(form, 'urgency')
    }

    showForm() {
        // Need to remember data on doctor change
        this.fillValues(this.regularContent, this.card)
        Object.values(this.content).forEach((obj) => this.fillValues(obj, this.card))
        // Create form
        const form = this.createHTMLElement()
        form.addEventListener(this.event, this.submit)
        form.setAttribute('data-form-id', this.card.id)
        // Insert category select in form
        const select = new Component({ ...pageElements.visitFormSelect, listener: this.selectCategory.bind(this, 'doctor') }).createHTMLElement()
        form.append(select)
        // Make chosen doctor selected
        this.setSelected(form, 'doctor')
        // Fill form by old data
        const category = Array.from(select.children).find(option => option.selected)
        const oldData = JSON.parse(JSON.stringify({ ...this.regularContent, ...this.content[category.value] }))
        this.fillValues(oldData, this.card)
        this.insertFields(oldData, form)
        this.setSelected(form, 'urgency')
        this.setValidation(form, 'INPUT')
        // Set status
        if (this.card.status === 'done') {
            form.querySelectorAll('[name="status"]').forEach(status => {
                if (status.value === 'done') status.checked = true
            })
        } else {
            form.querySelectorAll('[name="status"]').forEach(status => {
                if (status.value === 'open') status.checked = true
            })
        }
        // Inset form
        return form
    }

    async submit(e) {
        try {
            // Create obj to send
            const obj = super.submit(e)
            if (!obj) return
            // Close modal
            this.closest('.modal').remove()
            // Send request
            const data = await new DoctorAPIService().editCard(this.dataset.formId, obj)
            // Create new card
            const Visit = visitType[data.doctor]
            const newCard = new Visit(data).addCard()
            // Replace old card by new
            const oldCard = document.querySelector(`[data-id="${data.id}"]`)
            oldCard.replaceWith(newCard)
            // Delete loader
            deleteLoader()
        } catch (error) {
            console.error(error)
        }
    }
}

export class FilterVisitForm extends Form {
    constructor({ tagName, classList, value, attr, listener, event, formContent }) {
        super({ tagName, classList, value, attr, listener, event });
        this.content = formContent
    }

    showForm() {
        const form = this.createHTMLElement()
        form.addEventListener(this.event, this.filter.bind(this))
        form.addEventListener('reset', this.reset.bind(this))
        form.innerHTML = this.content
        return form
    }

    async reset() {
        
        const form = this.findHTMLElement()
        const filterCategory = super.submit.call(form);
        form.reset()
        if (Object.values(filterCategory).find(value => value !== '')) await showCards()
        deleteLoader()
    }

    async filter(e) {
        cards.prepend(showLoader())
        // Get all visits
        const visits = await new DoctorAPIService().getAllCards();
        // Clear field for cards
        cards.innerHTML = ''
        // Get data from form
        const form = this.findHTMLElement()
        const submit = Object.entries(super.submit.call(form, e));
        // Rename categories
        const filterCategoryAndValue = submit.map(([cat, val]) => {
            if (cat.includes('-')) return [cat.slice(0, cat.indexOf('-')), val]
            else return [cat, val]
        })
        // Get only unique categories
        const categories = [...new Set(filterCategoryAndValue.map(([cat, val]) => cat))]
        // Filter visits
        const filtredVisits = visits.filter(card => {
            return categories.every(category => {
                const filterValue = filterCategoryAndValue.filter(([cat, val]) => cat === category).map(([key, value]) => value).flat()
                if (filterValue.includes(card[category])) return true
                if (category === 'content' && (card.goal.includes(filterValue) || card.description.includes(filterValue))) return true
            })
        })
        // Insert filtred cards
        filtredVisits.forEach(obj => {
            let VisitClass = visitType[obj.doctor];
            const newCard = new VisitClass(obj).addCard()
            cards.append(newCard)
        })
        dragDrop()
        // Delete loader
        deleteLoader()
        // If no cards
        if (!cards.children.length) cards.innerHTML = pageElements.noMatches
    }
}
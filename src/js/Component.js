export class Component {

    constructor({tagName = "div", classList, value, attr = [], listener, event}) {
        this.tagName = tagName;
        this.classList = classList;
        this.value = value;
        this.attr = attr;
        this.listener = listener;
        this.event = event
    }

    createHTMLElement() {
        const elem = document.createElement(this.tagName);
        // Add class
        if (typeof this.classList === 'string') {
            elem.classList.add(this.classList)
        } else if (this.classList instanceof Array) {
            this.classList.forEach(className => elem.classList.add(className))
        }
        // Add value
        if (this.value !== undefined) {
            if (typeof this.value === 'string') {
                ['input', 'textarea', 'option'].includes(this.tagName) ? elem.value = this.value : elem.innerHTML = this.value;
            } else {
                elem.append(this.value)
            }
        }
        // Add attributes
        this.attr.forEach(obj => {
            if (obj instanceof Object) {
                elem.setAttribute(Object.entries(obj)[0][0], Object.entries(obj)[0][1])
            }
        })
        // Add event listener
        if (this.listener && this.event) {
            elem.addEventListener(this.event, this.listener)
        }
        // Insert new element
        return elem;
    }

    findHTMLElement() {
        let elem
        if (this.classList instanceof Array) {
            elem = document.querySelector('.' + this.classList.join('.'))
        } else {
            elem = document.querySelector('.' + this.classList)
        }
        return elem
    }

    static deleteElement(selector) {
        if (document.querySelector(selector)) document.querySelector(selector).remove()
    }

    static validateData(regExp, data) {
        return regExp.test(data)
    }
}
import { app, formType } from "./vars.js";
import { Component } from "./Component.js";

export class Modal extends Component {
    constructor({title, tagName = "div", classList = 'modal', value, attr = [], listener, event = "click"}) {
        super({tagName, classList, value, attr, listener, event})
        this.title = title
    }

    showModal(form) {
        // Create modal
        const modal = this.createHTMLElement()
        // Create close modal event
        modal.addEventListener('click', this.closeModal.bind(this))
        // Insert regular elements  in modal
        modal.insertAdjacentHTML('beforeend', `
            <div class="modal__body">
                <div class="modal__header">
                    <h3 class="modal__title">${this.title}</h3>
                    <button class="modal__close"></button>
                </div>
                <div class="modal__content"></div>
            </div>
        `)
        // Insert form in modal
        modal.querySelector('.modal__content').append(form)
        // Insert modal
        return modal
    }

    closeModal(e) {
        // Find modal
        const modal = this.findHTMLElement()
        // Close modal
        if (e.target === modal || e.target.classList.contains('modal__close')) modal.remove()
    }
}

